import React from "react";
import Aux from "../../hoc/Auxilliary";
import Toolbar from "../../components/Navigation/Toolbar/Toolbar";
const Layout = props => (
  <Aux>
    <Toolbar />
    <main>{props.children}</main>
  </Aux>
);

export default Layout;
