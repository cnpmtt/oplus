export default [
  {
    id: "1",
    name: "Data 1",
    level: "level 1",
    intro: "intro 1",
    time: 62,
    image: "http://lorempixel.com/640/480/nightlife",
    challenge_before: [],
    problems: [],
    topic: [{ id: "1", dataId: "1", name: "Topic 1" }],
    course: [
      {
        id: "1",
        dataId: "1",
        name: "Account Name1",
        account: "Account 1",
        challenges: "Challenges 1"
      }
    ]
  },
  {
    id: "2",
    name: "Data 2",
    level: "level 2",
    intro: "intro 2",
    time: 22,
    image: "http://lorempixel.com/640/480/fashion",
    challenge_before: [],
    problems: [],
    topic: [{ id: "2", dataId: "2", name: "Topic 2" }],
    course: [
      {
        id: "2",
        dataId: "2",
        name: "Account Name2",
        account: "Account 2",
        challenges: "Challenges 2"
      }
    ]
  },
  {
    id: "3",
    name: "Data 3",
    level: "level 3",
    intro: "intro 3",
    time: 32,
    image: "http://lorempixel.com/640/480/people",
    challenge_before: [],
    problems: [],
    topic: [{ id: "3", dataId: "3", name: "Topic 3" }],
    course: [
      {
        id: "3",
        dataId: "3",
        name: "Account Name3",
        account: "Account 3",
        challenges: "Challenges 3"
      }
    ]
  },
  {
    id: "4",
    name: "Data 4",
    level: "level 4",
    intro: "intro 4",
    time: 22,
    image: "http://lorempixel.com/640/480/sports",
    challenge_before: [],
    problems: [],
    topic: [{ id: "4", dataId: "4", name: "Topic 4" }],
    course: [
      {
        id: "4",
        dataId: "4",
        name: "Account Name4",
        account: "Account 4",
        challenges: "Challenges 4"
      }
    ]
  },
  {
    id: "5",
    name: "Data 5",
    level: "level 5",
    intro: "intro 5",
    time: 56,
    image: "http://lorempixel.com/640/480/people",
    challenge_before: [],
    problems: [],
    topic: [{ id: "5", dataId: "5", name: "Topic 5" }],
    course: [
      {
        id: "5",
        dataId: "5",
        name: "Account Name5",
        account: "Account 5",
        challenges: "Challenges 5"
      }
    ]
  },
  {
    id: "6",
    name: "Data 6",
    level: "level 6",
    intro: "intro 6",
    time: 87,
    image: "http://lorempixel.com/640/480/transport",
    challenge_before: [],
    problems: [],
    topic: [{ id: "6", dataId: "6", name: "Topic 6" }],
    course: [
      {
        id: "6",
        dataId: "6",
        name: "Account Name6",
        account: "Account 6",
        challenges: "Challenges 6"
      }
    ]
  },
  {
    id: "7",
    name: "Data 7",
    level: "level 7",
    intro: "intro 7",
    time: 52,
    image: "http://lorempixel.com/640/480/business",
    challenge_before: [],
    problems: [],
    topic: [{ id: "7", dataId: "7", name: "Topic 7" }],
    course: [
      {
        id: "7",
        dataId: "7",
        name: "Account Name7",
        account: "Account 7",
        challenges: "Challenges 7"
      }
    ]
  }
];
