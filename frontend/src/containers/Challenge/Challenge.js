import React, { useState, useEffect } from "react";
import Table from "../../components/Table";
import { Container, Row, Col } from "reactstrap";
import SearchBar from "../../components/SearchBar";
import CommonButton from "../../components/CommonButton";
import CaretDownIco from "../../assets/images/icons/caret-down-solid.svg";
import dummy from "./dummy";
import Loading from "../../components/Loading";

const Challenge = props => {
  const [fetched, setFetched] = useState(false);
  const [data, setData] = useState(null);
  useEffect(() => {
    if (fetched === false) {
      fetch("http://kmin.vn/auth/admin/challenges", {
        credentials: "omit",
        headers: {
          accept: "*/*",
          "accept-language": "en-US,en;q=0.9",
          "content-type": "application/json",
          authorization:
            "Kmin eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjI2MzI3ODg0OTEsImlkIjoxLCJvcmlnX2lhdCI6MTU1Mjc4ODQ5MSwicm9sZSI6ImFkbWluaXN0cmF0b3IifQ.sF84UyXR7y_WKVWCXlGpGPKQfpSKf7l8u9JI3XC_la4"
        },

        method: "GET",
        mode: "cors"
      })
        .then(response => response.json())
        .then(json => {
          console.log(json.data);
          setData(json.data);
          setFetched(true);
        });
    }

    console.log(dummy);
  });

  return (
    <Container>
      {/* Tach thanh nay ra 1 container */}
      <Row style={{ marginBottom: "30px", marginTop: "50px" }}>
        <Col md={4}>
          <SearchBar />
        </Col>
        <Col md={4}>
          <CommonButton color="primary" value={"Sort"} icon={CaretDownIco} />
        </Col>
      </Row>
      {fetched ? (
        <Table titles={["id", "challenge", "course", "action"]} data={data} />
      ) : (
        <Loading type="bars" />
      )}
    </Container>
  );
};

export default Challenge;
