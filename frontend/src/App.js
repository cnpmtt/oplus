import React, { Component } from "react";
import Layout from "./containers/Layout/Layout";
import { ThemeProvider } from "styled-components";
import "./App.css";

import theme from "./styles/themeStyle";
class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <Layout />
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
