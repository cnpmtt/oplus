import styled from "styled-components";

import { Button } from "reactstrap";
export default styled(Button)`
  display: flex !important;
  justify-content: center !important;
  align-items: center !important;
  min-width: 85px;
  text-transform: capitalize;
  /* max-height: 35px; */
  img {
    max-width: 15px;
    max-height: 20px;
    margin-right: 5px;
    color: white;
  }
`;
