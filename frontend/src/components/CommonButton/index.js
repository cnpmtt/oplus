import React from "react";
import classes from "./CommonButton.module.css";
import StyledButton from "./styled";
const CommonButton = props => {
  return (
    <StyledButton className={classes.CommonButton} color={props.color}>
      <img width={props.iconWidth} src={props.icon} />
      <span>{props.value}</span>
    </StyledButton>
  );
};

export default CommonButton;
