import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";
import styles from "./Toolbar.css";

import Challenge from "../../../containers/Challenge/Challenge";
const Toolbar = props => {
  const clItemHeader = styles.itemHeader;
  const clActive = {
    backgroundColor: "#33485f"
  };

  const Problem = () => (
    <div>
      <h2>Problem</h2>
      <hr />
      {/* IMPORT HERE */}
      {/* <Problem/> */}
    </div>
  );
  const ChallengeContainer = () => (
    <div>
      <h2>Challenge</h2>
      <hr />
      <Challenge />
    </div>
  );

  const Account = () => (
    <div>
      <h2>Account</h2>
      <hr />
      {/* IMPORT HERE */}
      {/* <Accounts/> */}
    </div>
  );

  const Result = () => (
    <div>
      <h2>Result</h2>
      <hr />
      {/* IMPORT HERE */}
      {/* <Result/> */}
    </div>
  );

  return (
    <Router>
      <div>
        <header className={styles.header}>
          <NavLink exact to="/" activeStyle={clActive} className={clItemHeader}>
            Challenge
          </NavLink>

          <NavLink
            to="/problem"
            activeStyle={clActive}
            className={clItemHeader}
          >
            Problem
          </NavLink>
          <NavLink
            to="/account"
            activeStyle={clActive}
            className={clItemHeader}
          >
            Account
          </NavLink>

          <NavLink to="/result" activeStyle={clActive} className={clItemHeader}>
            Result
          </NavLink>
        </header>

        <Route exact path="/" component={ChallengeContainer} />
        <Route path="/problem" component={Problem} />
        <Route path="/account" component={Account} />
        <Route path="/result" component={Result} />
      </div>
    </Router>
  );
};

export default Toolbar;
