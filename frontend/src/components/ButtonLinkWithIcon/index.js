/**
 *
 * ButtonLinkWithIcon
 *
 */

import React from "react";
import StyledButton from "./styled/StyledButton";
import PropTypes from "prop-types";

// import styled from 'styled-components';

const ButtonLinkWithIcon = props => {
  // eslint-disable-line react/prefer-stateless-function

  let text;
  if (props.children) {
    text = props.children;
  } else if (props.text) {
    text = props.text;
  }

  return (
    <StyledButton
      background={props.background}
      role="button"
      onClick={
        props.onClick ||
        (e => {
          e.preventDefault();
        })
      }
      tabIndex={0}
      className={`${props.className || ""}`}
      hasOnClick={!!props.onClick}
      onMouseOver={props.onMouseOver ? props.onMouseOver : () => true}
      onMouseOut={props.onMouseOut ? props.onMouseOut : () => true}
      onFocus={props.onFocus ? props.onFocus : () => true}
      onBlur={props.onBlur ? props.onBlur : () => true}
      textLeft={props.textLeft}
    >
      {props.textLeft && text}
      <img
        src={props.icon}
        width={props.width || 20}
        height={props.height || 20}
        alt={props.altIcon || "icon"}
      />
      {!props.textLeft && text}
    </StyledButton>
  );
};

ButtonLinkWithIcon.propTypes = {
  onClick: PropTypes.func,
  icon: PropTypes.string.isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
  altIcon: PropTypes.string,
  text: PropTypes.string,
  className: PropTypes.string,
  onMouseOver: PropTypes.func,
  onMouseOut: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  textLeft: PropTypes.bool,
  background: PropTypes.string
};

export default ButtonLinkWithIcon;
