import React from "react";
import ReactLoading from "react-loading";
import styles from "../../styles/themeStyle";
import styled from "styled-components";

const StyledLoading = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Loading = ({ type }) => (
  <StyledLoading>
    <ReactLoading type={type} color={styles.primaryBackground} width={75} />
  </StyledLoading>
);

export default Loading;
