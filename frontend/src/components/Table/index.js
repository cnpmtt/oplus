import React from "react";
import { Table } from "reactstrap";
import TableRow from "../TableRow";
import styled from "styled-components";
const StyledTable = styled(Table)`
  & > .TableHeader {
    text-transform: capitalize;
    font-weight: bold;
  }
`;

const TableContainer = props => {
  return (
    <StyledTable bordered>
      <thead className={"TableHeader"}>
        <tr>
          {props.titles.map(title => {
            return <td key={title}>{title}</td>;
          })}
        </tr>
      </thead>
      {props.data.map(challenge => {
        return <TableRow key={challenge.id} rowData={challenge} />;
      })}
    </StyledTable>
  );
};
export default TableContainer;
