import React, { Component } from "react";
import { InputGroup, InputGroupAddon, Input } from "reactstrap";

import SearchIco from "../../assets/images/icons/search-solid.svg";

import CommonButton from "../CommonButton";
const SearchBar = prop => {
  return (
    <InputGroup>
      <Input placeholder="Insert keyword..." />
      <InputGroupAddon addonType="append">
        <CommonButton
          color="primary"
          value="Search"
          icon={SearchIco}
          iconWidth={13}
        />
      </InputGroupAddon>
    </InputGroup>
  );
};

export default SearchBar;
