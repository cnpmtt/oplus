import React, { useState } from "react";
import ActionGroup from "../ActionGroup";
import { Col, Row } from "reactstrap";
import styled from "styled-components";
const StyledTableRow = styled.tbody`
  & .TableRow {
    & > td:last-child {
      max-width: 150px;
    }
    &:hover {
      background: #f5f5f5;
    }
    & img {
      max-width: 100%;
    }
  }
`;
const TableRow = props => {
  const [showDetail, setshowDetail] = useState(false);
  const handleShowInfo = () => {
    setshowDetail(!showDetail);
  };
  const { rowData } = props;
  return (
    <StyledTableRow key={rowData.id}>
      <tr className={"TableRow"} onClick={handleShowInfo}>
        <td scope="row">{rowData.id}</td>
        <td>{rowData.name}</td>
        <td>{rowData.course.name}</td>
        <td>
          <ActionGroup clone />
        </td>
      </tr>
      {showDetail ? (
        <tr className={"TableRow"}>
          <td colSpan={4}>
            <Row>
              <Col md={2}>
                <h6>Name:</h6>
                <input type="text" value={rowData.name} />
                <h6>Time:</h6>
                <input type="text" value={rowData.name} />
                <h6>Img:</h6>
                <img src={rowData.image} />
              </Col>
              <Col md={4}>
                <h6>Course:{rowData.course.name}</h6>
                <h6>Prechallenge:{rowData.challenge_before.length}</h6>
                <h6>Topic:{rowData.topic.name}</h6>
                <h6>Present:{rowData.intro}</h6>
              </Col>
              <Col md={6}>{"a"}</Col>
            </Row>
          </td>
        </tr>
      ) : (
        ""
      )}
    </StyledTableRow>
  );
};

export default TableRow;
