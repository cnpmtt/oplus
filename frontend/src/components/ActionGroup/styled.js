import styled from "styled-components";
export default styled.div`
  display: inline-flex;
  justify-content: center;
  button {
    margin: 0px 20px;
  }
`;
