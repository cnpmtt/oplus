import React from "react";
// import { ButtonGroup } from "reactstrap";
import createIco from "../../assets/images/icons/ic_create_24px.svg";
import cloneIco from "../../assets/images/icons/ic_flip_to_back_24px.svg";
import deleteIco from "../../assets/images/icons/ic_delete_24px.svg";
import CommonButton from "../CommonButton";
import StyledActionGroup from "./styled";
const ActionGroup = props => {
  return (
    <StyledActionGroup>
      <CommonButton value="edit" color="warning" icon={createIco} />
      {props.clone && (
        <CommonButton value="clone" color="info" icon={cloneIco} />
      )}
      <CommonButton value="delete" color="danger" icon={deleteIco} />
    </StyledActionGroup>
  );
};

export default ActionGroup;
